Getting Started
---------------

To get started with ProuDroid, you'll need to get
familiar with [Git and Repo](http://source.android.com/source/using-repo.html).

To initialize your local repository using the ProuDroid trees, use a command like this:

    repo init -u git@bitbucket.org:ProuDroid/android.git

To sync up:

    repo sync

